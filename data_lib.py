from sklearn import preprocessing
import numpy as np
import pandas as pd
import cv2

###Removing Background
def Nil_Background(image_path):
    image = cv2.imread(image_path,cv2.IMREAD_UNCHANGED)
    image_red = cv2.imread(image_path,cv2.IMREAD_UNCHANGED)
    image_red[:,:,0] = 0
    image_red[:,:,1] =0
    #image = image[200:600,400:700]
    #CONVERT IMAGE TO GRAYSCALE 
    image_gray = cv2.cvtColor(image_red, cv2.COLOR_BGR2GRAY)
    # onvert image to blck and white
    th1, image_edges = cv2.threshold(image_gray, 150, 255, cv2.THRESH_BINARY) #th=>threshold
    #########
    blur = cv2.GaussianBlur(image_gray,(5,5),0)
    ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    


    ########
    th2 = cv2.Canny(image_gray,100,255)
    # create background mask
    mask = np.zeros(image_red.shape, np.uint8)##
    # get most significant contours
    contours_draw, hierachy = cv2.findContours(th3, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    x = cv2.drawContours(mask, contours_draw, -1, (255, 255, 255), -1)
    good_image = cv2.bitwise_and(image, mask)##
    cv2.imshow('sdewfer',x)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return good_image

###Taking ta data
def data_x(image,axis):
    value=[]
    cl = np.sum(image,axis=axis)
    value.append(cl) #2d array
    value = value[0]
    normed_value = preprocessing.normalize([value])
    return value,normed_value 


###Setting the name for CSV file
def data_name(input_folder,individual_img):
    length = len(input_folder)
    name = individual_img[length:-4] #character number of directory till '.png'
    name = name.replace(' ','_')
    return name

###Saving the data
def data_saving(value,normed_value,name,colrow):
    df = pd.DataFrame({'Values': value,'N_values':normed_value[0]})
    df.to_csv('./data/{}_{}.csv'.format(name,colrow),index_label='steps')
    return 0


