import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit,root
from scipy.interpolate import UnivariateSpline
from lmfit import Model
###CSV Folder Import
inp_csv=('/content/CSV')

fwhm = []
esq =[]
cs =[]

for csv in glob.glob(inp_csv + '/*.csv'):
  df = pd.read_csv(csv)
  columns = np.array(df.steps)
  values = np.array(df['N_values'])

  ###Calculating Z_Score
  def z_score(intensity):
    mean_int = np.mean(intensity)
    std_int = np.std(intensity)
    z_scores = (intensity - mean_int) / std_int
    return z_scores

  threshold = 3
  ###Value with Z_Score
  v_z_score = np.array(abs(z_score(values)))

  def fixer(y,m):
    threshold = 3 # binarization threshold.
    spikes = abs(np.array(z_score(np.diff(y)))) > threshold
    y_out = y.copy() # So we don’t overwrite y 
    for i in np.arange(len(spikes)):
        if spikes[i] != 0: # If we have an spike in position i
            w = np.arange(i-m,i+1+m) # we select 2 m + 1 points around our spike
            w2 = w[spikes[w] == 0] # From such interval, we choose the ones which are not spikes
            y_out[i] = np.mean(y[w2]) # and we average their values
    return y_out

  fixer = fixer(values,m=3)
  def gaussian(x, amp, cen, wid):
    """1-d gaussian: gaussian(x, amp, cen, wid)"""
    l=(amp / (np.sqrt(2*np.pi) * wid)) * np.exp(-(x-cen)**2 / (2*wid**2))
    #l = amp*np.exp(-(x-cen)**2/(2*wid**2))
    return l


  gmodel = Model(gaussian)
  result = gmodel.fit(fixer,x=columns,amp = 50,cen=150,wid=50)
  
  amp = result.params['amp'].value
  cen = result.params['cen'].value
  wid = result.params['wid'].value
  std = wid
  y = result.best_fit

  ###Root Finding
  g = gaussian(columns,amp,cen,wid)
  fw = np.max(g)/2 #Y- value for FWHM
  ex2 = np.max(g)/np.exp(2) #Y-Value for 1/e^2
  spline = UnivariateSpline(columns, g-fw, s=0)
  spline1 = UnivariateSpline(columns,g-ex2, s=0)
  r1,r2 = spline.roots() #Roots of FWHM
  e1,e2 = spline1.roots() #Roots of 1/e^2
  fwidth = r2-r1 #Width for FWHM
  ewidth = e2-e1 #Width for 1/e^2
  fwhm.append(fwidth)
  #print(fwhm)
  esq.append(ewidth)
  #print(esq)
  csvv=csv[13:-4]
  cs.append(csvv)
  #print(cs)
dcsv = pd.DataFrame({'File Name':cs,'FWHM':fwhm,'e-2':esq})
#df = df[0:0] 
dcsv.to_csv('/content/All_Data_CSV/{}.csv'.format('Position_A'),index_label='steps')

print('Data for all images has produced!')

