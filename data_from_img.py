###This Code Is For Creating DataSet of Laser Beam Image###

###Libraries Used
import cv2
#import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import preprocessing
###
###
from data_lib import Nil_Background,data_x,data_name,data_saving


###Getting Datas
inputFolder = ('/media/pias/git/Laser_Beam_Image_Processing/')
#os.mkdir('Resized')
#os.mkdir('CSVs')

for image in glob.glob(inputFolder + '/*.png'):
    
    #Taking Image with No Background noise
    img = Nil_Background(image)###
    
    ###convert to gray
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    ##get data along column
    cols,cols_n = data_x(gray,0)
    
    ##get data along row
    row,row_n = data_x(gray,1)
    
    ###Taking Data Name
    name = data_name(inputFolder,image)
    
    ###Saving The Data as CSV
    columnn= data_saving(cols,cols_n, name, 'columns')
    row = data_saving(row, row_n, name, 'rows')

    ###Showing the Image(Not Important)

    cv2.imshow('img',img)
    k=cv2.waitKey(0)
    if k==27:
        break
    
cv2.destroyAllWindows()


###
print('The data has produces.ThankYou!')

