import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit,root
from scipy.interpolate import UnivariateSpline
from lmfit import Model

### load the data
df = pd.read_csv('./data/oo_rows.csv')

###transform the date to numpy array

columns = np.array(df.steps)
#columns = columns
values = np.array(df['N_values'])

###Plot The Data Without any modification
plt.plot(columns,values)
plt.title('')
plt.xlabel('steps')
plt.ylabel('pixel value')
plt.show()


###Calculating Z_Score
def z_score(intensity):
    mean_int = np.mean(intensity)
    std_int = np.std(intensity)
    z_scores = (intensity - mean_int) / std_int
    return z_scores

threshold = 3
###Value with Z_Score
v_z_score = np.array(abs(z_score(values)))

np.where(v_z_score != values,values,0)

###############
#############
###Ploting the data with Z_Score
plt.plot(columns, v_z_score)
plt.plot(columns, threshold*np.ones(len(columns)), label = 'threshold')
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.xlabel( 'steps' )
plt.ylabel( '|Z-Score|')
plt.show()


###Fixing the Spike
def fixer(y,m):
    threshold = 3 # binarization threshold.
    spikes = abs(np.array(z_score(np.diff(y)))) > threshold
    y_out = y.copy() # So we don’t overwrite y 
    for i in np.arange(len(spikes)):
        if spikes[i] != 0: # If we have an spike in position i
            w = np.arange(i-m,i+1+m) # we select 2 m + 1 points around our spike
            w2 = w[spikes[w] == 0] # From such interval, we choose the ones which are not spikes
            y_out[i] = np.mean(y[w2]) # and we average their values
    return y_out

fixer = fixer(values,m=3)
#df = pd.read_csv('/media/pias/git/Laser_Beam_Image_Processing/CSVs/33_rows.csv')
###Gaussian fitting
###Gaussian Fit Another
def gaussian(x, amp, cen, wid):
    """1-d gaussian: gaussian(x, amp, cen, wid)"""
    l=(amp / (np.sqrt(2*np.pi) * wid)) * np.exp(-(x-cen)**2 / (2*wid**2))
    #l = amp*np.exp(-(x-cen)**2/(2*wid**2))
    return l


gmodel = Model(gaussian)
result = gmodel.fit(fixer,x=columns,amp = 50,cen=150,wid=50)



amp = result.params['amp'].value
cen = result.params['cen'].value
wid = result.params['wid'].value
std = wid
y = result.best_fit

#print(result.eval(x=300))
#print(result.fit_report())
print(result.fit_report())
#########

#########


### Ploting the final data 
plt.plot(columns, values, label = 'original data',color='yellow')
plt.plot(columns, fixer, label = 'fixed spectrum',color='blue')
#plt.plot(columns, result.init_fit, '--', label='initial fit',color ='blue')
#plt.plot(columns, result.best_fit, '-', label='best fit',color='red')

#dely = result.eval_uncertainty(sigma=3)
#plt.fill_between(columns, result.best_fit-dely, result.best_fit+dely, color="#ABABAB",
#                 label='3-$\sigma$ uncertainty band')
plt.title('',fontsize = 20)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.xlabel('steps')
plt.ylabel('pixel value')
plt.legend()
plt.show()


####error calculation
####error calculation 
def MAPE(Y_actual,Y_Predicted):
    mape = np.mean(np.abs((Y_actual - Y_Predicted)/Y_actual))*100
    return mape

error = MAPE(fixer,result.best_fit)
#print(error)


####is it good?
def gausss(x,amp,cen,wid):
    g = amp/(np.sqrt(2*np.pi)*wid)*np.exp(-(x-cen)**2/(2*wid**2))
    return g


###Root Finding
g = gausss(columns,amp,cen,wid)

plt.plot(columns,g)
plt.show()



fw = np.max(g)/2 #Y- value for FWHM
ex2 = np.max(g)/np.exp(2) #Y-Value for 1/e^2
spline = UnivariateSpline(columns, g-fw, s=0)
spline1 = UnivariateSpline(columns,g-ex2, s=0)
r1,r2 = spline.roots() #Roots of FWHM
e1,e2 = spline1.roots() #Roots of 1/e^2
print('Roots for FWHM::',r1,r2)
print('Roots for 1/e^2 ::',e1,e2)
fwidth = r2-r1 #Width for FWHM
ewidth = e2-e1 #Width for 1/e^2
print('Diameter for FWHM ::',fwidth)
print('Diameter for 1/e^2 ::',ewidth)

#FWHM from formula
fwhm=(2*np.sqrt(2*np.log(2))*wid)
print('FWHM from Formula::',fwhm)
print('1/e^2 from formula::',4*wid)
plt.plot(columns,g,color='green')
#plt.axvspan(r1, r2, facecolor='g', alpha=0.5)
plt.plot([r1,r2],[fw,fw],label='FWHM',color = 'red')
plt.plot([e1,e2],[ex2,ex2],label='1/e^2',color = 'blue')
plt.xlabel('steps')
plt.ylabel('pixel value')

plt.legend()
plt.show()











